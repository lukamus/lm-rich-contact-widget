<?php
/**
 * Plugin Name: Rich Contact Widget Extension
 * Plugin URI: https://lukamus@bitbucket.org/lukamus/lm-rich-contact-widget
 * Description: Add additional fields to Rich Contact Widget (dependency)
 * Version: 0.1
 * License: GPL
 * Author: Luke Morton
 * Author URI: http://lukemorton.com.au
 * 
 * Copyright 2012  Luke Morton  (email: luke.morton@internode.net.au)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2, as 
 * published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

// Add additional keys
function lm_add_widget_keys( $keys ) {
    $new_keys = array( 
        'po_box',
        'po_zip',
        'po_city',
        'po_state',
        'po_country',
        'mobile',
        'fax',
        'note' );
    array_splice( $keys, 10, 0, $new_keys );
    return $keys;
}
add_filter( 'rc_widget_keys', 'lm_add_widget_keys' );

// Add additional form elements
function lm_add_widget_form_output( $widget_form_output, $instance ) {
    global $wp_widget_factory;

    // New fields to add to admin widget form
    $fields = array(
        'po_box' => 'PO Box : ',
        'po_zip' => 'PO Box Postal/ZIP code : ',
        'po_city' => 'PO Box City : ',
        'po_state' => 'PO Box State : ',
        'po_country' => 'PO Box Country : ',
        'mobile' => 'Mobile number : ',
        'fax' => 'Fax number : ',
        'note' => 'Notes (like ABN) : ' );

    // Loop through new fields and add at the end of the widget
    foreach ( $fields as $key => $value ) {
        $id = $wp_widget_factory->widgets['RC_Widget']->get_field_id( $key );
        $nm = $wp_widget_factory->widgets['RC_Widget']->get_field_name( $key );
        if ( $key === 'note' ) {
            $widget_form_output .= '<p>
                <label for="' . $id . '">' . $value . '</label>
                <textarea class="widefat" id="' . $id . '" name="' . $nm  . '"
                    type="text">' .  esc_textarea( $instance[ $key ] ) . '</textarea>
                </p>';
        } else {
            $widget_form_output .= '<p>
                <label for="' . $id . '">' . $value . '</label>
                <input class="widefat" id="' . $id . '" name="' . $nm  . '"
                    type="text" value="' . esc_attr( $instance[ $key ] ) . '" />
                </p>';
        }
    }

    // Return the updated widget (string of html)
    return $widget_form_output;
}
add_filter( 'rc_widget_form_output', 'lm_add_widget_form_output', 10, 2 );

// Add additional output
function lm_add_widget_output( $widget_output, $instance ) {
    global $wp_version;

    // Parse $widget_output as DOM string
    $doc = new DOMDocument();
    $doc->loadHTML( $widget_output );
    $xpth = new DOMXPath( $doc );

    // Add post office box address
    if ( !empty ( $instance[ 'po_box' ] ) ) {
        $format = '<div class="adr po" itemprop="address">
            <span class="post-office-box">%s</span><br />
            <span class="locality">%s</span>,
            <span class="region">%s</span>
            <span class="postal-code">%s</span><br />
            <span class="country-name">%s</span></div>';
        $node_value = sprintf(
            $format,
            $instance[ 'po_box' ],
            $instance[ 'po_city' ],
            $instance[ 'po_state' ],
            $instance[ 'po_zip' ],
            $instance[ 'po_country' ] );
        $parent = $xpth->query( '//ul[@class="vcard"]' );
        $next = $xpth->query( '//li[@class="tel"]' );
        $node = $doc->createElement( 'li' );
        $fragment = $doc->createDocumentFragment();
        $fragment->appendXML( $node_value );
        $node->appendChild( $fragment );
        $parent->item( 0 )->insertBefore( $node, $next->item( 0 ) );
    }

    // TODO: Find a more flexible way of inserting at the right point.
    // TODO: Use the insertion method above below.
    $ref_node = $xpth->query( '//li[@class="email"]' )->item( 0 );

    // New items to add
    $keys = array(
        'mobile' => array( 'tel', 'telephone' ),
        'fax' => array( 'tel', 'telephone' ),
        'note' => array( 'note', 'note' ) );

    $node_value = null;

    // Loop over items to add and insert
    foreach ( $keys as $key => $value ) {
        switch ( $key ) {
            case 'mobile';
                if ( !empty ( $instance[ $key ]) ) {
                    if ( version_compare( $wp_version, '3.4', '>=' ) && 
                            wp_is_mobile() ) {
                        $format = '<a href="tel:%s">%s</a>';
                        $inst = $instance[ $key ];
                        $node_value = sprintf( $format, $inst, $inst );
                    } else {
                        $node_value = $instance[$key];
                    }
                }
                break;
            default; // fax, note
                $node_value = !empty ( $instance[$key] ) ? nl2br( $instance[$key] ) : '';
                break;

        }
        // Create node and add new data
        if ( !empty ( $node_value ) ) {
            $node = $doc->createElement( 'li' );
            $node->setAttribute( 'class', $value[0] . ' ' . $key );
            $node->setAttribute( 'itemprop', $value[1] );
            $node->nodeValue = $node_value;
            //$ref_node->parentNode->insertBefore( $node, $ref_node );

            $parent = $xpth->query( '//ul[@class="vcard"]' );
            $next = $xpth->query( '//li[@class="email"]' );
            $node = $doc->createElement( 'li' );
            $fragment = $doc->createDocumentFragment();
            $fragment->appendXML( $node_value );
            $node->appendChild( $fragment );
            $parent->item( 0 )->insertBefore( $node, $next->item( 0 ) );
        }
    }

    // Return updated output
    $widget_output = $doc->saveXML( $parent->item( 0 ) );
    return $widget_output;
}
add_filter( 'rc_widget_output', 'lm_add_widget_output', 10, 2 );
