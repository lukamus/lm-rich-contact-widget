README
======

Extend the Rich Contact Widget with additional fields:

    * Mobile (cell) phone number
    * Fax number
    * Notes field
